var client = null, api = null, irc = null, app = {}, gui = require('nw.gui'), win = gui.Window.get(), fs = require('fs'), bot = angular.module('botApp', ["oc.lazyLoad"]);

bot.controller('botCtrl', function($scope, $compile, configService, $ocLazyLoad){
    // Isolation variable for this controller
    var _self = this;
    _self.config = configService;
    // Load the config file
    
    fs.readFile("bot.conf", function(err, data) {
        if(!err) {
            /**
             * if error is null, the we can continue with the rest of the initalization of the bot
             */
            
            _self.config.data = JSON.parse(data); // Parse the config data to a json object
            $scope.$apply(); // Force the _self.config changes to apply

            var clientOptions = { // Twitch IRC server connection details
                options: {
                    exitOnError : false
                },
                connection : {
                    preferredServer : '192.16.64.152',
                    preferredPort : '443'
                },
                identity : {
                    username : _self.config.data.connection.username,
                    password : _self.config.data.connection.password
                },
                channels : _self.config.data.connection.channels.split(',')
            };
                
            /**
             * Connect to the twitch IRC server and log a successful connection
             */
            irc = require('twitch-irc');

            client = new irc.client(clientOptions);
            client.connect();
            client.addListener('connected', function (address, port) {
                console.log('connected to ' + clientOptions.channels[0] + ' at ' + address + ':' + port);
            });

        
        } else {
            /**
             * No file found or something else has gone wrong. 
             * We will rewrite the error if the error code equals 'ENOENT',
             * just so it doesn't appear there is a file missing and say something more appropriate.
             */
        }
    });
}).factory('configService', function(){ return { // Global config service
    data : { // Default config data
        connection : {
            username : 'YOUR-USERNAME',
            password : 'YOUR-PASSWORD',
            channels : 'YOUR-CHANNEL'
        }
    },
    save : function() { // Save config function
        fs.writeFile("bot.conf", JSON.stringify(this.data));
    }
} });