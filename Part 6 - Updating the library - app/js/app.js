var client = null, api = null, irc = null, app = {}, gui = require('nw.gui'), win = gui.Window.get(), fs = require('fs'), bot = angular.module('botApp', ["oc.lazyLoad"]);

bot.controller('botCtrl', function($scope, $compile, configService, $ocLazyLoad){
	// Isolation variable for this controller
	var _self = this;
	_self.config = configService;
	// Load the config file
	
	fs.readFile("bot.conf", function(err, data) {
		if(!err) {
            /**
             * if error is null, the we can continue with the rest of the initalization of the bot
             */
			
			_self.config.data = JSON.parse(data); // Parse the config data to a json object
			$scope.$apply(); // Force the _self.config changes to apply

			var clientOptions = { // Twitch IRC server connection details
				options: {
					exitOnError : false,
					debug : true
				},
			    connection : {
			        preferredServer : '192.16.64.152',
			        preferredPort : '443'
			    },
			    identity : {
			        username : _self.config.data.connection.username,
			        password : _self.config.data.connection.password
			    },
			    channels : _self.config.data.connection.channels.split(',')
			};
                
            /**
             * Connect to the twitch IRC server and log a successful connection
             */
			//irc = require("tmi-js"); // Commented out irc require due to inclusion of the CDN script
		    
		    client = new irc.client(clientOptions);
		    client.connect();
		    client.addListener('connected', function (address, port) {
			    console.log('connected to ' + clientOptions.channels[0] + ' at ' + address + ':' + port);
			});

			_self.lazyLoad('spamProtection'); // Call the lazy load function to load in the spamProtection module
		} else {
            /**
             * No file found or something else has gone wrong. 
             * We will rewrite the error if the error code equals 'ENOENT',
             * just so it doesn't appear there is a file missing and say something more appropriate.
             */
		}
	});

	/**
	 * Lazyload function to load in specific modules
	 */
	_self.lazyLoad = function(file) {
		$ocLazyLoad.load('js/'+file+'.js').then(function() { // Attempt to load in the supplied module javascript file
			console.log('Loaded ' + file)

			/**
			 * Compile containing controller div and add in the lazy element using tmp as the path to our html file (not including the .html extension)
			 */
			var elToAppend = $compile($('<div>').attr('ng-controller', file + 'Ctrl as ' + file).html('<lazy tmp="views/'+file+'"></lazy>'))($scope);
			el = angular.element('#contain');
			el.append(elToAppend);
		}, function(e) {
			/**
			 * Lazy load failed
			 */
			console.error('Failed to load ' + file + ': ' + e);
		});
	}
	

}).factory('configService', function(){ return { // Global config service
	data : { // Default config data
		connection : {
            username : 'YOUR-USERNAME',
            password : 'YOUR-PASSWORD',
            channels : 'YOUR-CHANNEL'
        }
	},
	save : function() { // Save config function
		fs.writeFile("bot.conf", JSON.stringify(this.data));
	}
} }).directive('lazy', function() { // Angular directive to give our <lazy> element purpose
	return {
		restrict : 'E', // Restrict this directive to elements only
		link : function(scope, element, attrs) {
			scope.getContentUrl = function() {
				return attrs.tmp + '.html'; // return the supplied template file html filename
			}
		},
		template : '<div ng-include="getContentUrl()"></div>', // The template to be introduced into the <lazy> element
		replace : true // Remove the <lazy> element leaving the template in its place
	}
});

