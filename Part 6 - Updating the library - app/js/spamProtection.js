angular.module('botApp', []).controller('spamProtectionCtrl', function($scope, configService){
	// Isolation variable for this controller
	var _self = this;
	_self.config = configService;

	/**
	 * Listen for the chat event on the client
	 */
	client.addListener('chat', function(ch, user, msg) {
		var urlRegex = /(www\.|https?:\/\/)?[a-zA-Z0-9]{2,254}\.[a-zA-Z0-9]{2,4}(\S*)/gi; // URL Regex
		console.log('we are listening')
		if(urlRegex.test(msg)) { // If the message captured contains a link or email
			console.log('is link')
		    if(user['user-type'] !== 'mod' && ch.replace('#', '') !== user.username) { // If the user is not a mod and is not the channel broadcaster
		    	console.log('here');
		    	client.timeout(ch, user.username, 1); // Time the user out for 1 second to purge their chat
		    	client.say(ch, 'Tut Tut ' + user['display-name'] + ' you know you are not allowed to post links.');
		    }
		}
	})
});