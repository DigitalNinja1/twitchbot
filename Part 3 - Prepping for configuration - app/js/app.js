var client = null, api = null, irc = null, app = {}, gui = require('nw.gui'), win = gui.Window.get(), fs = require('fs'), bot = angular.module('botApp', []);

bot.controller('botCtrl', function($scope){
    // Isolation variable for this controller
    var _self = this;

    fs.readFile("bot.conf", function(err, data) { // Attempt to read the bot config file
        if(!err) {
            /**
             * if error is null, the we can continue with the rest of the initalization of the bot
             */
            var clientOptions = { // Twitch IRC server connection details
                options: {
                    exitOnError : false
                },
                connection : {
                    preferredServer : '192.16.64.152',
                    preferredPort : '443'
                },
                identity : {
                    username : 'YOUR-USERNAME',
                    password : 'oauth:YOUR-PASSWORD'
                },
                channels : ['YOUR-CHANNEL-NAME']
            };
			
            /**
             * Connect to the twitch IRC server and log a successful connection
             */
            irc = require('twitch-irc');
			
            client = new irc.client(clientOptions);
            client.connect();
            client.addListener('connected', function (address, port) {
                console.log('connected to ' + clientOptions.channels[0] + ' at ' + address + ':' + port);
            });
        } else {
            /**
             * No file found or something else has gone wrong. 
             * We will rewrite the error if the error code equals 'ENOENT',
             * just so it doesn't appear there is a file missing and say something more appropriate.
             */
        }
    });
});