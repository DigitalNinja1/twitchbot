angular.module('botApp', []).controller('spamProtectionCtrl', function($scope, configService){
	// Isolation variable for this controller
	var _self = this;
	_self.config = configService;

	/**
	 * Listen for the chat event on the client
	 */
	client.addListener('chat', function(ch, user, msg) {
		var urlRegex = /(www\.|https?:\/\/)?[a-zA-Z0-9]{2,254}\.[a-zA-Z0-9]{2,4}(\S*)/gi; // URL Regex
		if(urlRegex.test(msg)) { // If the message captured contains a link or email
			var mods = client.mods(ch); // Capture an array of channel moderators
		    if(mods.indexOf(user.username) < 0 && user.special.indexOf('broadcaster') < 0) { // If the user is not a mod and is not the channel broadcaster
		    	client.timeout(ch, user.username, 1); // Time the user out for 1 second to purge their chat
		    }
		}
	})
});