var client = null, api = null, irc = null;

var clientOptions = {
	options: {
		exitOnError : false
	},
    connection : {
        preferredServer : '192.16.64.152', // Twitch IRC server address
        preferredPort : '443' // Twitch IRC server port
    },
    identity : {
        username : 'YOUR-USERNAME', // Your bots username
        password : 'oauth:YOUR-PASSWORD' // Your bots oauth password obtained from http://twitchapps.com/tmi/
    },
    channels : ['YOUR-CHANNEL'] // An array of channel names you wish to connect to
};

(function() {
    irc = require('twitch-irc'); // IRC library object

    client = new irc.client(clientOptions); // New client connection to the IRC server
    client.connect();
    client.addListener('connected', function (address, port) { // Listening out for the 'connected' event
	    console.log('connected to ' + clientOptions.channels[0] + ' at ' + address + ':' + port);
	});
	client.addListener('chat', function(ch, user, msg) { // Listening out for the 'chat' event
		$('<p>').html('<span style="color:' + user.color + '">' + user.username + '</span>: ' + msg).appendTo('body'); // Write the message into the body with the username
		if(msg.substr(0,1) == "!") { // Checking the message to see if it starts with an !, this denotes a command call to the bot
			var commandMsg = msg.substr(1).toLowerCase().split(' ').shift(); // Getting the command name by stripping the !, exploding by spaces and pulling out the first word
			if(commandMsg == 'hello') {
				client.say(ch, 'Hello to you too ' + user.username); // If the command is 'hello', use the say command to reply to the user
			}
		}
	});
})();